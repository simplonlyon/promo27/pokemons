import type { Pokemon } from '@/entities'
import axios from 'axios'

export async function fetchPokemons() {
  const response = await axios.get<Pokemon[]>('https://pokebuildapi.fr/api/v1/pokemon')
  return response.data
}
