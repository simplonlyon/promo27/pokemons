export interface Pokemon {
  id: number
  name: string
  image: string
  apiTypes: [
    {
      name: string
      image: string
    }
  ]
}
